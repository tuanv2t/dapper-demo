﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DapperDemo.BlogDB.Test
{
    public static class FileConfigReader
    {
        public static string BlogConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["BlogConnectionString"].ConnectionString; }
        }
    }
}
