﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DapperDemo.BlogDB.Test
{
    [TestClass]
    public class PostRepositoryTest
    {
        [TestMethod]
        public void QRolllbackTranInsertPostUseSavePoint_OK()
        {
            //Note: run the script in Sample Database\Blog DB Example.sql to create database first
            var rep = new PostRepository(FileConfigReader.BlogConnectionString);
            rep.RolllbackTranInsertPostUseSavePoint();

        }
        [TestMethod]
        public void InsertPostWithOutputDemo_OK()
        {
            var rep = new PostRepository(FileConfigReader.BlogConnectionString);
            var result = rep.InsertPostWithOutputDemo();
            Assert.AreEqual(result.ReturnedCode,3);
        }


    }
}
