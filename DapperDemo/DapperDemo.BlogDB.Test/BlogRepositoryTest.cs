﻿using System;
using System.Threading.Tasks;
using DapperDemo.BlogDB.POCO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DapperDemo.BlogDB.Test
{
    [TestClass]
    public class BlogRepositoryTest
    {
        [TestMethod]
        public void QueryAll_OK()
        {
            //Note: run the script in Sample Database\Blog DB Example.sql to create database first
            var rep = new BlogRepository(FileConfigReader.BlogConnectionString);
            var allBlogData = rep.QueryAll();
            Assert.IsNotNull(allBlogData);

        }
        [TestMethod]
        public async Task QueryAllAsync_OK()
        {
            //Note: run the script in Sample Database\Blog DB Example.sql to create database first
            var rep = new BlogRepository(FileConfigReader.BlogConnectionString);
            var allBlogData = await rep.QueryAllAsync();
            Assert.IsNotNull(allBlogData);

        }
        [TestMethod]
        public void QueryIdTitle_OK()
        {
            //Note: run the script in Sample Database\Blog DB Example.sql to create database first
            var rep = new BlogRepository(FileConfigReader.BlogConnectionString);
            var allBlogData = rep.QueryIdTitle();
            Assert.IsNotNull(allBlogData);

        }
        [TestMethod]
        public void Insert_OK()
        {
            //Note: run the script in Sample Database\Blog DB Example.sql to create database first
            var rep = new BlogRepository(FileConfigReader.BlogConnectionString);
            //Insert a new Blog
            var blog = new Blog()
            {
                Title = "Bolg-" + DateTime.Now.ToString(),
                OwnerID = 1,
                CreatedDate = DateTime.Now
            };
            rep.Insert(blog);
            Assert.IsTrue(blog.BlogID >0);

        }
    }
}
