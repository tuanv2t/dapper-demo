USE [BLog]
GO

/****** Object:  StoredProcedure [dbo].[InsertPostWithExceptionDemo]    Script Date: 3/26/2017 10:51:00 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[InsertPostWithOutputDemo]
@ReturnedCode INT OUT
	,@ReturnedError NVARCHAR(MAX) OUT
AS
BEGIN
     	SET XACT_ABORT
		,NOCOUNT ON
	SET @ReturnedError = '';
	SET @ReturnedCode = 0;


	DECLARE @TranCounter INT;

	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;

	BEGIN TRY
		
	   -- #A	
	   -- Try to update one record of Blog, after the error occured, try to check if the 
	   -- record has been updated or not ( it should not be updated )
	   UPDATE Blog
	   SET CreatedDate = GETDATE()
	   WHERE BlogID = 1 

	   --#B
	   -- Try to issue an error
	    INSERT Post (
                    BlogID
                    ,Tittle
                    ,CreatedDate
                    )
            VALUES (
                    NULL -- Try to make an exception here
                    ,'Post xyz'
                    ,GETUTCDATE()
                    )

		SET @ReturnedError = 'OK'
		SET @ReturnedCode = 1
		
	END TRY

	BEGIN CATCH
		SET @ReturnedCode = 3 -- there are some error

		SELECT @ReturnedError = error_message()
		
	END CATCH
	IF @ReturnedCode = 1
	BEGIN
		IF @TranCounter = 0
			COMMIT TRANSACTION;
	END
	ELSE
	BEGIN
		IF @TranCounter = 0 BEGIN
			ROLLBACK TRANSACTION; END
		ELSE IF XACT_STATE() <> - 1
			BEGIN 
			ROLLBACK TRANSACTION ProcedureSave 
			
			END;
	END


	
	  
	
 
           


END



GO


