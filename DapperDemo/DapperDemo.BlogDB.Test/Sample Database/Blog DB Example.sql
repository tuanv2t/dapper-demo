CREATE DATABASE [BLog]
GO
USE Blog
GO
CREATE TABLE Blog (
       BlogID INT IDENTITY(1, 1)
       ,Title NVARCHAR(500)
       ,OwnerID INT NOT NULL
       ,CreatedDate DATETIME
       ,PRIMARY KEY (BlogID)
       )
GO
 
CREATE TABLE Post (
       PostID INT IDENTITY(1, 1)
       ,BlogID INT NOT NULL
       ,Tittle NVARCHAR(400)
       ,CreatedDate DATETIME
       ,PRIMARY KEY (PostID)
       ,CONSTRAINT [FK_Post_Blog] FOREIGN KEY ([BlogID]) REFERENCES [dbo].[Blog]([BlogID])
       )
GO
 
CREATE TABLE Comment (
       CommentID INT IDENTITY(1, 1)
       ,PostID INT NOT NULL
       ,CommentDate DATETIME
       ,Content NVARCHAR(MAX)
       ,PRIMARY KEY (CommentID)
       ,CONSTRAINT [FK_Comment_Post] FOREIGN KEY ([PostID]) REFERENCES [dbo].[Post]([PostID])
       )
GO
 
 
CREATE VIEW MyRand
AS
SELECT RAND() AS RandomVal

GO
CREATE FUNCTION dbo.fnRandomString (@length INT)  
RETURNS VARCHAR(MAX)  
AS  
BEGIN  
    declare @alphabet varchar(36) = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
	DECLARE @result VARCHAR(MAX)
	DECLARE @rand float
	SET @result = ''
	DECLARE @Count INT
	SET @Count = 1
	WHILE @Count < @length
	BEGIN
		--SET @result = @result + substring(@alphabet, convert(int, rand()*36), 1) -- Unable to use rand() inside a function
		-- workaround, create a view return a random number
		SELECT @rand = RandomVal from MyRand
		SET @result = @result + substring(@alphabet, convert(int, @rand*36), 1)
		SET @Count = @Count + 1
	END 

return @result;
END;  

GO

CREATE FUNCTION dbo.fnRandomInt (@min INT, @max INT)  
RETURNS INT  
AS  
BEGIN  
	DECLARE @result INT
	DECLARE @rand float
	SELECT @rand = RandomVal from MyRand

	SELECT @result = FLOOR(@rand*(@max-@min)+@min);

	
return @result;
END;  
GO
DECLARE @NumberOfBlog INT
SET @NumberOfBlog = 5000

IF NOT EXISTS (
              SELECT *
              FROM Blog
              )
BEGIN
    DECLARE @Count INT
	SET @Count = 1
	WHILE @Count <= @NumberOfBlog -- It's able to custom the number of records here
	BEGIN
		INSERT INTO Blog (
				  Title
				  ,OwnerID
				  ,CreatedDate
				  )
		   VALUES (
				  --N'Blog- ' + dbo.fnRandomString(10)
				  N'Blog- ' + CONVERT(VARCHAR,@Count)
				  ,1
				  ,GETDATE()
				  )
		SET  @Count = @Count + 1
	END 

END
GO
 
-- Init Post
DECLARE @NumberOfPost INT
DECLARE @NumberOfBlog INT 
SELECT @NumberOfBlog = (SELECT COUNT(*) FROM Blog)
--SET @NumberOfPost = @NumberOfBlog * 2
SET @NumberOfPost = @NumberOfBlog 
IF NOT EXISTS (
              SELECT *
              FROM Post
              )
BEGIN
 DECLARE @Count INT
	SET @Count = 1
	DECLARE @BlogID INT

	WHILE @Count <= @NumberOfPost -- It's able to custom the number of records here
	BEGIN
	   -- Insert post for random blog

	 --SELECT @BlogID = dbo.fnRandomInt(1,@NumberOfBlog)

	 INSERT INTO Post (
              BlogID
              ,Tittle
              ,CreatedDate
              )
       VALUES (
              --@BlogID
			  @Count
              --,N'Post- ' + dbo.fnRandomString(10)
			  ,N'Post- ' + CONVERT(VARCHAR,@Count)
              ,GETDATE()
              )

	SET @Count = @Count + 1
   END
END
GO
 
 -- Init Comment
 DECLARE @NumberOfComment INT
 DECLARE @NumberOfPost INT 
SELECT @NumberOfPost = (SELECT COUNT(*) FROM Post)
-- SET @NumberOfComment = @NumberOfPost * 2
SET @NumberOfComment = @NumberOfPost * 2


IF NOT EXISTS (
              SELECT *
              FROM Comment
              )
BEGIN
 DECLARE @PostID INT

  DECLARE @Count INT
	SET @Count = 1

	WHILE @Count <= @NumberOfPost -- It's able to custom the number of records here
	BEGIN
	   -- Insert post for random blog

	 SELECT @PostID = dbo.fnRandomInt(1,@NumberOfPost)
	 INSERT INTO Comment (
              PostID
              ,Content
              ,CommentDate
              )
       VALUES (
              --@PostID
			  @Count
              --,N'Comment - ' + dbo.fnRandomString(100)
			  ,N'Comment- ' + CONVERT(VARCHAR,@Count)
              ,GETDATE()
              )

	SET @Count = @Count + 1
   END


      
END
GO


CREATE PROCEDURE [dbo].[RolllbackTranInsertPost_UseSavePoint]
AS
BEGIN
       SET NOCOUNT ON
 
       DECLARE @TranCounter INT;
 
       SET @TranCounter = @@TRANCOUNT;
 
       IF @TranCounter > 0
              -- Procedure called when there is
              -- an active transaction.
              -- Create a savepoint to be able
              -- to roll back only the work done
              -- in the procedure if there is an
              -- error.
              SAVE TRANSACTION ProcedureSave;
       ELSE
              -- Procedure must start its own
              -- transaction.
              BEGIN TRANSACTION;
 
       -- Modify database.
       BEGIN TRY
              DECLARE @BlogID INT
 
              SELECT @BlogID = MAX(BlogID)
              FROM Blog
 
              INSERT Post (
                     BlogID
                     ,Tittle
                     ,CreatedDate
                     )
              VALUES (
                     NULL -- Try to make an exception here
                     ,'Post xyz'
                     ,GETUTCDATE()
                     )
 
              -- Get here if no errors; must commit
              -- any transaction started in the
              -- procedure, but not commit a transaction
              -- started before the transaction was called.
              IF @TranCounter = 0
                     -- @TranCounter = 0 means no transaction was
                     -- started before the procedure was called.
                     -- The procedure must commit the transaction
                     -- it started.
                     COMMIT TRANSACTION;
       END TRY
 
       BEGIN CATCH
              -- An error occurred; must determine
              -- which type of rollback will roll
              -- back only the work done in the
              -- procedure.
              IF @TranCounter = 0
                     -- Transaction started in procedure.
                     -- Roll back complete transaction.
                     ROLLBACK TRANSACTION;
              ELSE
              -- Transaction started before procedure
              -- called, do not roll back modifications
              -- made before the procedure was called.
              IF XACT_STATE() <> - 1
                     -- If the transaction is still valid, just
                     -- roll back to the savepoint set at the
                     -- start of the stored procedure.
                     ROLLBACK TRANSACTION ProcedureSave;
 
              -- If the transaction is uncommitable, a
              -- rollback to the savepoint is not allowed
              -- because the savepoint rollback writes to
              -- the log. Just return to the caller, which
              -- should roll back the outer transaction.
              -- After the appropriate rollback, echo error
              -- information to the caller.
              DECLARE @ErrorMessage NVARCHAR(4000);
              DECLARE @ErrorSeverity INT;
              DECLARE @ErrorState INT;
 
              SELECT @ErrorMessage = ERROR_MESSAGE();
 
              SELECT @ErrorSeverity = ERROR_SEVERITY();
 
              SELECT @ErrorState = ERROR_STATE();
 
              RAISERROR (
                           @ErrorMessage
                           ,-- Message text.
                           @ErrorSeverity
                           ,-- Severity.
                           @ErrorState -- State.
                           );
       END CATCH
END

GO



 

