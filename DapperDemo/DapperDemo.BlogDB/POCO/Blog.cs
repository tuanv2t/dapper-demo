﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DapperDemo.BlogDB.POCO
{
    public class Blog
    {
        [DapperKey]
        public int BlogID { get; set; }
        public string Title { get; set; }
        public int OwnerID { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
