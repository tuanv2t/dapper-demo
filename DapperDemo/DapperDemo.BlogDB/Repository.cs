﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DapperDemo.BlogDB
{
    public class Repository
    {
        //Note: run the script in DapperDemo.BlogDB.Test\Sample Database\Blog DB Example.sql to create database first
        protected string _connectionString;
        public Repository(string connectionString)
        {
            this._connectionString = connectionString;
        }
    }
}
