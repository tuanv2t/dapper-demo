﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DapperDemo.BlogDB
{
    public class StoredProcExeResultModel
    {
        public int ReturnedCode { get; set; }
        public string ReturnedError { get; set; }
    }
}
