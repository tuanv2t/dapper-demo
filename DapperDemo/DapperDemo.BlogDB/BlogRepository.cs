﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using DapperDemo.BlogDB.POCO;
using DapperDemo;
namespace DapperDemo.BlogDB
{
    public class BlogRepository: DapperRepositoryBase
    {
        public BlogRepository(string connectionString) : base(connectionString)
        {
            
        }
        public List<Blog> QueryAll()
        {
            string query = "select * from Blog";
            using (var conn = new SqlConnection(this.ConnectionString))
            {
                //https://github.com/StackExchange/Dapper
                //Execute a query and map the results to a strongly typed List
                var data = (List<Blog>)conn.Query<Blog>(query);
                return data;
            }
        }
        public async Task<IEnumerable<Blog>> QueryAllAsync()
        {
            string query = "select * from Blog";
            using (var conn = new SqlConnection(this.ConnectionString))
            {
                //https://github.com/StackExchange/Dapper
                //Execute a query and map the results to a strongly typed List
                return await conn.QueryAsync<Blog>(query);
            }
        }
        public List<Blog> QueryIdTitle()
        {
            string query = "select BlogID, Title from Blog";
            using (var conn = new SqlConnection(this.ConnectionString))
            {
                //https://github.com/StackExchange/Dapper
                //Execute a query and map the results to a strongly typed List
                var data = (List<Blog>)conn.Query<Blog>(query);
                return data;
            }
        }

        public void Insert(Blog newBlog)
        {
            this.Insert<Blog>(newBlog);
            if (newBlog.BlogID <= 0)
            {
                throw  new Exception("Unable to insert blog");
            }
        }
    }
}
