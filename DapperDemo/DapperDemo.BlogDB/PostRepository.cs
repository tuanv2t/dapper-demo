﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using DapperDemo.BlogDB.POCO;

namespace DapperDemo.BlogDB
{
    public class PostRepository:Repository
    {
        public PostRepository(string connectionString) : base(connectionString)
        {
            
        }

        public void RolllbackTranInsertPostUseSavePoint()
        {
            using (var conn = new SqlConnection(this._connectionString))
            {
                conn.Execute("RolllbackTranInsertPost_UseSavePoint", null, null, null, CommandType.StoredProcedure);
            }
        }
        public StoredProcExeResultModel InsertPostWithOutputDemo()
        {
            //Run script DapperDemo.BlogDB.Test\Sample Database\InsertPostWithOutputDemo
            DynamicParameters p = new DynamicParameters();
            p.Add("@ReturnedCode", dbType: DbType.Int32, direction: ParameterDirection.Output);
            //p.Add("@ReturnedError", dbType: DbType.String, direction: ParameterDirection.Output);//There will be an error if Without size 
            p.Add("@ReturnedError", dbType: DbType.String, direction: ParameterDirection.Output, size: 4000);
            using (var conn = new SqlConnection(this._connectionString))
            {
                conn.Execute("InsertPostWithOutputDemo",p,null,null, CommandType.StoredProcedure);
                var result = new StoredProcExeResultModel();
                result.ReturnedCode = p.Get<int>("@ReturnedCode");
                result.ReturnedError = p.Get<string>("@ReturnedError");
                return result;
            }
        }
    }
}
